package br.com.itau.Payment.controllers;

import br.com.itau.Payment.models.Payment;
import br.com.itau.Payment.models.dto.CreatePaymentRequest;
import br.com.itau.Payment.models.dto.CreatePaymentResponse;
import br.com.itau.Payment.models.dto.GetPaymentResponse;
import br.com.itau.Payment.models.dto.PaymentMapper;
import br.com.itau.Payment.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentMapper paymentMapper;


    @PostMapping
    public CreatePaymentResponse create(@RequestBody @Valid CreatePaymentRequest createPaymentRequest) {
        Payment payment = paymentMapper.toPayment(createPaymentRequest);

        payment = paymentService.create(payment);

        return paymentMapper.toCreatePaymentResponse(payment);
    }

    @GetMapping("/{id}")
    public GetPaymentResponse getById(@PathVariable Long id) {
        Payment customer = paymentService.getById(id);

        return paymentMapper.toGetPaymentResponse(customer);
    }

}
