package br.com.itau.Payment.creditCardClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="CREDIT-CARD")
public interface CreditCardClient {

    @GetMapping("/card_number/{card_number}")
    CreditCard getByCardNumber(@RequestParam("card_number")  String cardNumber);

    @GetMapping("/{id}")
    CreditCard getById(@PathVariable Long id);
}
