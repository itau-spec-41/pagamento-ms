package br.com.itau.Payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePaymentRequest {
    @JsonProperty("card_number")
    private String cardNumber;

    private Double value;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
