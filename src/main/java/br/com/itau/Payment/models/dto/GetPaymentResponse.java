package br.com.itau.Payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class GetPaymentResponse {
    private Long id;
    @JsonProperty("card_number")
    private String cardNumber;
    private Double value;
    @JsonProperty("transaction_time")
    private LocalDate transactionTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDate getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(LocalDate transactionTime) {
        this.transactionTime = transactionTime;
    }
}
