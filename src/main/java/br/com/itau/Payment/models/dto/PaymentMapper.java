package br.com.itau.Payment.models.dto;

import br.com.itau.Payment.models.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {

    public Payment toPayment(CreatePaymentRequest createPaymentRequest) {
        Payment payment = new Payment();
        payment.setCardNumber(createPaymentRequest.getCardNumber());
        payment.setValue(createPaymentRequest.getValue());
        return payment;
    }

    public CreatePaymentResponse toCreatePaymentResponse(Payment payment) {
        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();
        createPaymentResponse.setCardNumber(payment.getCardNumber());
        createPaymentResponse.setValue(payment.getValue());
        createPaymentResponse.setTransactionTime(payment.getTransactionTime());
        return createPaymentResponse;
    }

    public GetPaymentResponse toGetPaymentResponse(Payment payment) {
        GetPaymentResponse getPaymentResponse = new GetPaymentResponse();
        getPaymentResponse.setId(payment.getId());
        getPaymentResponse.setCardNumber(payment.getCardNumber());
        getPaymentResponse.setTransactionTime(payment.getTransactionTime());
        getPaymentResponse.setValue(payment.getValue());
        return getPaymentResponse;
    }

}
