package br.com.itau.Payment.repositories;

import br.com.itau.Payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

}
