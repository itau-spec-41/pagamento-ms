package br.com.itau.Payment.services;

import br.com.itau.Payment.creditCardClient.CreditCard;
import br.com.itau.Payment.creditCardClient.CreditCardClient;
import br.com.itau.Payment.exceptions.PaymentNotFoundException;
import br.com.itau.Payment.models.Payment;
import br.com.itau.Payment.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    private CreditCardClient creditCardClient;

    @Autowired
    private PaymentRepository paymentRepository;

    public Payment create(Payment payment) {
        CreditCard creditCard = creditCardClient.getByCardNumber(payment.getCardNumber());

//        if(creditCard.isAtivo())
//        payment.setCardNumber(creditCard.getCardNumber());
        payment.setTransactionTime(LocalDate.now());

        return paymentRepository.save(payment);
    }

//    public Payment update(Payment updatedCreditCard) {
//        Payment creditCard = getById(updatedCreditCard.getId());
//
//        creditCard.setActive(updatedCreditCard.getActive());
//
//        return creditCardRepository.save(creditCard);
//    }

    public Payment getById(Long id) {
        Optional<Payment> byId = paymentRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PaymentNotFoundException();
        }

        return byId.get();
    }
}
